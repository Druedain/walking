extern crate walking;

use walking::Boundaries;
use walking::rbox_walking;
use walking::point::Point;

fn main() {
    let boundaries = Boundaries::up_down_left_right(2, 12, 3, 10);
    let rbox_walking_factory = rbox_walking::Factory::new();
    let rbox_walking = rbox_walking_factory.restricted_by(boundaries);

    rbox_walking.draw_boundaries();
    rbox_walking.at_coordinates(1, boundaries.down + 2).draw("'q' to quit");
    rbox_walking.start();
}
