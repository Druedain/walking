use std::cell::Cell;

use rustbox::Key;
use boundaries::{Boundaries, Inside};
use position::{Position, Direction};
use action::Action;
use continuation::Continuation;

pub struct Walking {
    boundaries: Boundaries,
    boundaries_inside: Inside,
    position: Cell<Position>,
}

impl Walking {
    pub fn restricted_by(boundaries: Boundaries) -> Walking {
        let position = boundaries.make_starting_position();
        let boundaries_inside = Inside::new(boundaries);

        Walking {
            boundaries: boundaries,
            boundaries_inside: boundaries_inside,
            position: Cell::new(position),
        }
    }

    pub fn on_pressed(&self, key: Key) -> Action {
        if let Key::Char('q') = key {
            return Action::Continuation { continuation: Continuation::Quit };
        }

        let direction = Walking::translate_arrow(key);
        if let Some(direction) = direction {
            return Action::Direction { direction: direction };
        }

        Action::Continuation { continuation: Continuation::NextIteration }
    }

    fn translate_arrow(key: Key) -> Option<Direction> {
        match key {
            Key::Down => Some(Direction::Down),
            Key::Up => Some(Direction::Up),
            Key::Left => Some(Direction::Left),
            Key::Right => Some(Direction::Right),
            _ => None,
        }
    }

    pub fn position_moved_to(&self, direction: &Direction) -> Position {
        self.position().moved_to(direction)
    }

    pub fn position(&self) -> Position {
        self.position.get()
    }

    pub fn move_position_to(&self, direction: &Direction) {
        let moved = self.position.get().moved_to(direction);
        self.position.set(moved);
    }

    pub fn vertical_boundaries(&self) -> Vec<usize> {
        (self.boundaries.up..self.boundaries.down + 1).collect()
    }

    pub fn horizontal_boundaries(&self) -> Vec<usize> {
        (self.boundaries.left..self.boundaries.right + 1).collect()
    }

    pub fn left_boundary(&self) -> usize {
        self.boundaries.left
    }

    pub fn right_boundary(&self) -> usize {
        self.boundaries.right
    }

    pub fn top_boundary(&self) -> usize {
        self.boundaries.up
    }

    pub fn bottom_boundary(&self) -> usize {
        self.boundaries.down
    }

    pub fn horizontal_inside(&self) -> &Vec<usize> {
        &self.boundaries_inside.horizontal_inside
    }

    pub fn vertical_inside(&self) -> &Vec<usize> {
        &self.boundaries_inside.vertical_inside
    }

    pub fn contains(&self, position: Position) -> bool {
        self.boundaries_inside.contains(position)
    }
}
