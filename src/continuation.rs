#[derive(PartialEq)]
pub enum Continuation {
    Quit,
    NextIteration,
}
