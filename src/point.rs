pub trait Point {
    fn draw(&self, symbol: &str);
}
