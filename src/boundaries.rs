use position::Position;

#[derive(Copy, Clone)]
pub struct Boundaries {
    pub up: usize,
    pub down: usize,
    pub left: usize,
    pub right: usize,
}

impl Boundaries {
    pub fn up_down_left_right(up: usize, down: usize, left: usize, right: usize) -> Self {
        Boundaries {
                up: up,
                down: down,
                left: left,
                right: right,
            }
            .validated()
    }

    fn validated(self) -> Boundaries {
        if self.down < self.up + 2 {
                Err(format!("Horizontal boundaries too small: <{}, {}>.",
                            self.up,
                            self.down))
            } else {
                Ok(self)
            }
            .and_then(|_| if self.right < self.left + 2 {
                Err(format!("Vertical boundaries too small: <{}, {}>.",
                            self.left,
                            self.right))
            } else {
                Ok(self)
            })
            .unwrap()
    }

    pub fn make_starting_position(&self) -> Position {
        Position {
            vertical: self.up + 1,
            horizontal: self.left + 1,
        }
    }
}

pub struct Inside {
    pub horizontal_inside: Vec<usize>,
    pub vertical_inside: Vec<usize>,
}

impl Inside {
    pub fn new(boundaries: Boundaries) -> Inside {
        Inside {
            horizontal_inside: (boundaries.left + 1..boundaries.right).collect(),
            vertical_inside: (boundaries.up + 1..boundaries.down).collect(),
        }
    }

    pub fn contains(&self, position: Position) -> bool {
        self.horizontal_inside.contains(&position.horizontal) &&
        self.vertical_inside.contains(&position.vertical)
    }
}
